<?php

namespace Ideallia\CollectiShop\Platform\Installer;

use Ideallia\CollectiShop\Core\Installer\AbstractInstaller;
use Ideallia\CollectiShop\Core\Installer\Runners\PreflightRunner;
use Ideallia\CollectiShop\Customer\Console\Commands\InstallCollectiCrmCommand;
use Ideallia\CollectiShop\Payment\Console\Commands\InstallCollectiPayCommand;
use Ideallia\CollectiShop\Platform\Installer\Runners\ShopRunner;
use Ideallia\CollectiShop\Product\Console\Commands\InstallCollectiPimCommand;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;

class CollectiShopInstaller extends AbstractInstaller
{
    /**
     * The runner classes that should be executed.
     *
     * @var array
     */
    protected $runners = [
        'preflight' => PreflightRunner::class,
        'shops' => ShopRunner::class,
    ];

    /**
     * The dependency classes that should be executed
     *
     * @var array
     */
    protected $dependencies = [
        "collectishop:crm:install" => InstallCollectiCrmCommand::class,
        "collectishop:pay:install" => InstallCollectiPayCommand::class,
        "collectishop:pim:install" => InstallCollectiPimCommand::class,
        "collectishop:ship:install" => InstallCollectiShipCommand::class,
    ];

    /**
     * Run the installer.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function run(): void
    {
        if (!$this->command) {
            throw new \Exception('You must attach a command instance to the installer');
        }

        $this->installCore();

        $this->getRunners()->each(function ($runner) {
            $runner = app()->make($runner);
            $runner->onCommand($this->command);
            $runner->run();
            $runner->after();
        });

        $this->command->info('Installation of CollectiShop complete');

        $this->command->newLine();

        $this->command->info("Ready to run installers of dependencies");
        $this->getDependencies()->each(function ($InstallerCommand, $cliCommand) {
            if (class_exists($InstallerCommand) && is_a($InstallerCommand, Command::class, true)) {
                $this->command->call($cliCommand);
                $this->command->newLine();
            }
        });

        $this->command->info('Installation of dependencies complete');
    }

    /**
     * Get dependencies
     *
     * @return Collection
     */
    public function getDependencies(): Collection
    {
        return collect(array_merge(
            $this->dependencies,
            config('collectishop.installer.dependencies', [])
        ));
    }

    /**
     * Get runners
     *
     * @return Collection
     */
    public function getRunners(): Collection
    {
        return collect(array_merge(
            $this->runners,
            config('collectishop.installer.runners', [])
        ));
    }
}
