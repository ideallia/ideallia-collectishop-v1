<?php

namespace Ideallia\CollectiShop\Platform\Installer\Runners;

use Ideallia\CollectiShop\Core\Installer\Contracts\InstallRunnerContract;
use Ideallia\CollectiShop\Core\Installer\Runners\AbstractRunner;
use Ideallia\CollectiShop\Core\Models\Countries\Country;
use Ideallia\CollectiShop\Customer\Models\Addresses\Address;
use Ideallia\CollectiShop\Platform\Models\Shops\Shop;
use Ideallia\CollectiShop\Platform\Models\Shops\ShopAddress;
use Illuminate\Support\Facades\DB;

class ShopRunner extends AbstractRunner implements InstallRunnerContract
{
    /**
     * Run
     *
     * @return ShopRunner
     * @throws \Exception
     */
    public function run(): ShopRunner
    {
        if (!DB::table(Shop::table())->count()) {
            $this->installShops();
        } elseif ($this->command->confirm("Would you like to reinstall the shops?")) {
            if ($this->removeShops()) {
                $this->installShops();
            }
        }

        return $this;
    }

    /**
     * Install shops
     *
     * @return void
     * @throws \Exception
     */
    protected function installShops(): void
    {
        if ($this->checkIfAddANewShop()) {
            $this->installShop();
        }
    }

    protected function installShop()
    {
        $Shop = Shop::create();
        $Shop->fill([
            'name' => $this->command->ask('What is the name of the shop which you\'d like to add?'),
            'default' => $this->command->confirm('Would you like to make this the default shop?', false),
            'enabled' => $this->command->confirm('Would you like to publish this shop?', true),
        ]);
        $isSaved = $Shop->save();
        if ($isSaved && class_exists(Address::class)) {
            $Shop->refresh();

            $Countries = Country::all();
            $countryChoices = array();
            foreach ($Countries as $Country) {
                $countryChoices[$Country->getId()] = $Country->getAttributeValue("code");
            }

            $street = $this->command->ask('What is the street name of the shop?');
            $housenumber = $this->command->ask('What is the house number of the shop?', 1);
            $suffix = $this->command->ask('What is the house number suffix of the shop?');
            $postalCode = $this->command->ask('What is the postal code of the shop?');
            $region = $this->command->ask('What is the region in which the shop is located?');
            $city = $this->command->ask('What is the city of the shop?');
            do {
                $countryCode = $this->command->choice(
                    'What is the country code of the shop?',
                    array_values($countryChoices)
                );
                $Country = Country::where('code', $countryCode)->firstOrFail();
            } while (false === ($Country instanceof Country));
            $countryId = $Country->getId();

            $Address = Address::firstOrNew([
                'country_id' => $countryId,
                'postal_code' => $postalCode,
                'housenumber' => $housenumber,
                'suffix' => $suffix,
            ]);
            $Address->fill([
                'street' => $street,
                'housenumber' => $housenumber,
                'suffix' => $suffix,
                'postal_code' => $postalCode,
                'region' => $region,
                'city' => $city,
                'country_id' => $countryId,
            ]);
            if ($Address->save()) {
                $Address->refresh();
                $ShopAddress = ShopAddress::create();
                $ShopAddress->fill([
                    'shop_id' => $Shop->getId(),
                    'address_id' => $Address->getId(),
                ]);
                $ShopAddress->save();
            }
            if ($this->command->getOutput()->isVerbose()) {
                $this->command->line('The shop has been saved successfully');
            }
            if ($this->checkIfAddANewShop()) {
                $this->installShop();
            }
            return;
        } elseif ($isSaved) {
            if ($this->command->getOutput()->isVerbose()) {
                $this->command->line('The shop has been saved successfully');
            }
            if ($this->checkIfAddANewShop()) {
                $this->installShop();
            }
            return;
        }
        $this->command->getOutput()->error('Problem saving shop');
    }

    /**
     * Check if a new shop needs to be added
     *
     *
     * @return bool
     */
    protected function checkIfAddANewShop(): bool
    {
        return $this->command->confirm('Would you like to add a new shop?', true);
    }

    /**
     * Remove shops
     *
     * @return bool
     */
    protected function removeShops(): bool
    {
        try {
            DB::table(ShopAddress::table())->truncate();
            DB::table(Shop::table())->truncate();
            if ($this->command->getOutput()->isVerbose()) {
                $this->command->info("All shops have been deleted from the database");
            }
            return true;
        } catch (\Exception $exception) {}

        $this->command->getOutput()->error('Problem with deleting shops');
        return false;
    }
}
