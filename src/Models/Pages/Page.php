<?php

namespace Ideallia\CollectiShop\Platform\Models\Pages;

use Ideallia\CollectiShop\Core\Models\Base\BaseModel;
use Illuminate\Database\Query\Builder;

class Page extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'title',
        'introduction',
        'content',
        'access',
        'seo_title',
        'seo_introduction',
        'seo_keywords',
        'enabled',
    ];

    public function scopeParent(Builder $query, Page $Page): Builder
    {
        return $query->where('page_id', $Page->getId());
    }

    public function scopeAccess(Builder $query, $access): Builder
    {
        return $query->where('access', $access);
    }
}
