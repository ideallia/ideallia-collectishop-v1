<?php

namespace Ideallia\CollectiShop\Platform\Models\Shops;

use Ideallia\CollectiShop\Core\Models\Base\BaseModel;
use Illuminate\Database\Query\Builder;

class Shop extends BaseModel
{
    protected $table = 'shop_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'default',
        'enabled',
    ];
}
