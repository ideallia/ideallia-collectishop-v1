<?php

namespace Ideallia\CollectiShop\Platform\Models\Shops;

use Ideallia\CollectiShop\Core\Models\Base\BaseModel;

class ShopAddress extends BaseModel
{
    protected $table = 'shop_address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'address_id',
    ];
}
