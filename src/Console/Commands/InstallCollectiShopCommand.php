<?php

namespace Ideallia\CollectiShop\Platform\Console\Commands;

use Ideallia\CollectiShop\Platform\Installer\CollectiShopInstaller;
use Ideallia\CollectiShop\Core\Installer\Events\PreflightCompletedEvent;
use Illuminate\Console\Command;
use Illuminate\Contracts\Events\Dispatcher;

class InstallCollectiShopCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collectishop:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install CollectiShop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param Dispatcher $events Events
     * @param CollectiShopInstaller $installer Installer
     *
     * @return void
     * @throws \Exception
     */
    public function handle(Dispatcher $events, CollectiShopInstaller $installer)
    {
        $this->printTitle();
        $events->listen(PreflightCompletedEvent::class, function ($event) {
            $database = $event->response['database'];
            if (! $database['connected']) {
                $this->error('Unable to connect to database');

                exit(1);
            }
        });

        // Run the installer...
        $installer->onCommand($this)->run();

        $this->comment('CollectiShop has been successfully installed');
    }

    /**
     * Print the title.
     *
     * @return void
     */
    protected function printTitle()
    {
        $this->line('= Welcome to ====================================');
        $this->line('   ______      ____          __  _ _____ __
  / ____/___  / / /__  _____/ /_(_) ___// /_  ____  ____
 / /   / __ \/ / / _ \/ ___/ __/ /\__ \/ __ \/ __ \/ __ \
/ /___/ /_/ / / /  __/ /__/ /_/ /___/ / / / / /_/ / /_/ /
\____/\____/_/_/\___/\___/\__/_//____/_/ /_/\____/ .___/
  / __ \/ ___/   / ____/    _________  ____ ___ /_/__ ___  ___  _____________
 / / / /\__ \   / __/______/ ___/ __ \/ __ `__ \/ __ `__ \/ _ \/ ___/ ___/ _ \
/ /_/ /___/ /  / /__/_____/ /__/ /_/ / / / / / / / / / / /  __/ /  / /__/  __/
\____//____/  /_____/ ____\___/\____/_/ /_/ /_/_/ /_/ /_/\___/_/   \___/\___/
   / __ \/ /___ _/ /_/ __/___  _________ ___
  / /_/ / / __ `/ __/ /_/ __ \/ ___/ __ `__ \
 / ____/ / /_/ / /_/ __/ /_/ / /  / / / / / /
/_/   /_/\__,_/\__/_/  \____/_/  /_/ /_/ /_/');
        $this->line(str_pad(' '.$this->getVersion().' =', 48, '=', STR_PAD_LEFT));
    }

    /**
     * Get version
     *
     * @return string
     */
    private function getVersion(): string
    {
        // return CollectiShop::version();
        return "v1.0.0";
    }
}
