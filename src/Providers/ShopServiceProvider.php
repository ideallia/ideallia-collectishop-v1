<?php

namespace Ideallia\CollectiShop\Platform\Providers;

use Ideallia\CollectiShop\Platform\Console\Commands\InstallCollectiShopCommand;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
//use Validator;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadProviders();
        $this->loadTranslations();
        $this->mapValidators();
        $this->publishConfig();
        $this->mapValidators();
        $this->mapBindings();
        $this->registerMiddleware();
        $this->mapCommands();
        $this->loadMigrations();
        Gate::before(function ($user) {
            return $user->hasRole('admin') ? true : null;
        });
    }

    /**
     * Load up our module providers.
     *
     * @return void
     */
    protected function loadProviders(): void
    {
        $providers = [
        ];
        foreach ($providers as $provider) {
            $this->app->register($provider, true);
        }
    }

    /**
     * Load translations
     *
     * @return void
     */
    protected function loadTranslations(): void
    {
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'collectishop');
    }

    /**
     * Publish config
     *
     * @return void
     */
    protected function publishConfig(): void
    {
        $this->publishes([
            __DIR__.'/../../config/collectishop.php' => config_path('collectishop.php'),
        ], 'config');

        $this->mergeConfigFrom(
            __DIR__.'/../../config/thirdparty.php', 'thirdparty'
        );
    }

    /**
     * Load migrations.
     *
     * @return void
     */
    protected function loadMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    }

    /**
     * Extend our validators.
     *
     * @return void
     */
    protected function mapValidators(): void
    {
    }

    /**
     * Map commands
     *
     * @return void
     */
    public function mapCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCollectiShopCommand::class,
            ]);
        }
    }

    /**
     * Do our application bindings.
     *
     * @return void
     */
    protected function mapBindings()
    {
//        $this->app->singleton('currency_converter', function ($app) {
//            return $app->make(CurrencyConverter::class);
//        });
//
//        $this->app->singleton('api', function ($app) {
//            return $app->make(Factory::class);
//        });
//
//        $mediaDrivers = config('assets.upload_drivers', []);
//
//        $this->app->singleton(GetCandy::class, function ($app) {
//            return new GetCandy;
//        });
//
//        foreach ($mediaDrivers as $name => $driver) {
//            $this->app->singleton($name.'.driver', function ($app) use ($driver) {
//                return $app->make($driver);
//            });
//        }
    }

    /**
     * Register our middleware.
     *
     * @return void
     */
    protected function registerMiddleware()
    {
//        $this->app['router']->aliasMiddleware('api.currency', SetCurrencyMiddleware::class);
//        $this->app['router']->aliasMiddleware('api.customer_groups', SetCustomerGroups::class);
//        $this->app['router']->aliasMiddleware('api.locale', SetLocaleMiddleware::class);
//        $this->app['router']->aliasMiddleware('api.tax', SetTaxMiddleware::class);
//        $this->app['router']->aliasMiddleware('api.channels', SetChannelMiddleware::class);
//        $this->app['router']->aliasMiddleware('api.detect_hub', DetectHubRequestMiddleware::class);
    }
}
