<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_item', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('page_id')->unsigned()->nullable();
            $table->foreign('page_id')
                ->references('id')
                ->on('page_item')
                ->nullOnDelete();
            $table->string('title');
            $table->text('introduction');
            $table->text('content');
            $table->string('access')->unique();
            $table->string('seo_title')->nullable();
            $table->tinyText('seo_description')->nullable();
            $table->tinyText('seo_keywords')->nullable();
            $table->boolean('enabled')->default(true)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_item');
    }
}
