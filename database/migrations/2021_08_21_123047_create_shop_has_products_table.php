<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopHasProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('product_item')) {
            Schema::create('shop_has_products', function (Blueprint $table) {
                $table->integer('shop_id')->unsigned();
                $table->bigInteger('product_id')->unsigned()->nullable();
                $table->foreign('product_id')->references('id')->on('product_item')->nullOnDelete();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('product_item')) {
            Schema::dropIfExists('shop_has_products');
        }
    }
}
