<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('address_item')) {
            Schema::create('shop_address', function (Blueprint $table) {
                $table->id();
                $table->bigInteger('shop_id')->unsigned()->nullable();
                $table->foreign('shop_id')->references('id')->on('shop_item')->nullOnDelete();
                $table->bigInteger('address_id')->unsigned();
                $table->foreign('address_id')->references('id')->on('address_item')->onDelete('restrict');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('address_item')) {
            Schema::dropIfExists('shop_address');
        }
    }
}
