<p align="center">
  <a href="https://www.collectishop.com/"><img src="https://www.collectishop.com/logo.svg" width="310" alt="CollectiShop"></a>
</p>

# CollectiShop OS E-commerce platform #

This is the home for the newly open source e-commerce platform in development.
You will find more information here in a later moment.

### Current version ###

Current version: 0.0.1

First release version: 1.0.0

### 📖 How do I get set up? ###

For installation instructions and usage details, please take a look at the **[official guides](https://docs.collectishop.com/)**.

### 📄 License ###
CollectiShop is open-sourced software licensed under the [Apache License 2.0](LICENSE)

### Who do I talk to? ###

If you've questions or want to join this project, send a mail to **[development@collectishop.com](mailto:development@collectishop.com)**.